//
//  NSString_MD5.h
//  NSString+MD5
//
//  Created by  on 12/5/7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5_Extension)
- (NSString *)md5;
@end
